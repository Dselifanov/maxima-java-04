package org.example;

public class Train extends Transport {

    public Train(String name, int capacity, int speed, float costOfKm) {
        super(name, capacity, speed, costOfKm);
    }
    String id;
    int carriageCount;
    boolean isExpress;

    public int getPrice(City city) {
        int costOfTrans;
        if (!city.hasAirport() )
            return 0;

        costOfTrans = (int) (this.getCostOfKm() * city.getDistanceKm());

        return costOfTrans;
    }
}
